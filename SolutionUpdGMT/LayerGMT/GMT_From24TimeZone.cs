﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using com.paginar.johnson.UpdateGMT.Generic;

namespace com.paginar.johnson.UpdateGMT
{

    internal class GMT_From24TimeZone : IQueryGMT<ObjectResult<float?>, float?>, IImplementLogger
    {
        private Dictionary<string, Struct_Url_Idioma> UrlCountry;
        public IParserByGMT<ObjectResult<string>, string> ParserHTML=null;
        

        public GMT_From24TimeZone(Dictionary<string, Struct_Url_Idioma> _CountryByUrl)
        {
            UrlCountry = _CountryByUrl;
        }

        public ObjectResult<float?> GetValueGMT(string QueryCountry)
        {
          //  if (QueryCountry.ToLower().IndexOf("venezuela") >= 0) System.Diagnostics.Debugger.Break();

            ObjectResult<float?> valueGMT = null;

            try
            {
                using (WebClient webC = new WebClient())
                {
                    webC.Headers.Add(HttpRequestHeader.AcceptCharset, "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
                    webC.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                    webC.Headers.Add(HttpRequestHeader.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                    webC.Headers.Add(HttpRequestHeader.AcceptLanguage, "es-es,es;q=0.8,en-us;q=0.5,en;q=0.3");

                    this.Logger.Log(string.Format("Procesando pais: {0}", QueryCountry));
                    byte[] bytes = webC.DownloadData(UrlCountry[QueryCountry].Url);
                    string html = Encoding.UTF8.GetString(bytes);
                    string _idiom = UrlCountry[QueryCountry].Idioma;

                    float? _valRet = this.ParserHtmlForValueGMT(html, _idiom);
                    valueGMT = new ObjectResult<float?>(_valRet, _valRet.HasValue);

                }
            }
            catch (Exception error)
            {
                string str=string.Format("Ocurrio un error al intentar extraer el valor GMT para '{0}'. Error: {1}",QueryCountry,error.Message);
                this.Logger.Log(str); 
                valueGMT = new ObjectResult<float?>(null, false);
                
            }

            return valueGMT;
        }


    
    

        private float? ParserHtmlForValueGMT(string html,string idioma_contenido)
        {
            Logger.Log("Obteniendo valor de GMT...");
            this.ParserHTML.SourceExpRegular = new com.paginar.johnson.UpdateGMT.XmlReader.Match_24TimeZone(idioma_contenido);

            ObjectResult<string> valueGMT = this.ParserHTML.ParserResult(html);
            //if (string.IsNullOrEmpty(valueGMT))
            if(!valueGMT.IsValid)
            {
                this.Logger.Log("No se encontro un valor de GMT");
                return null;
            }
            else
            {

                
                float valueResult = this.ConvertStringToNumericValueValid(valueGMT.Result);
                this.Logger.Log(string.Format("Valor obtenido: {0}", valueResult));
                return valueResult;
            }

            
        }

        private float ConvertStringToNumericValueValid(string valueGMT)
        {
            // Invariante: se considera que valueGMT tiene un valor flotante.

            string[] test1 = valueGMT.Split(':');
            float resultadoFinal = 0;
            if (test1.Length > 1)
            {
                float gmtEntero = float.Parse(test1[0]);
                float _gmtDecimal = float.Parse(test1[1]);
                if (_gmtDecimal != 0)
                {
                    float aux1_parteDecimal = _gmtDecimal / 60;
                    float signo = (gmtEntero > 0 ? +1 : -1);
                    resultadoFinal = Math.Abs(gmtEntero) + Math.Abs(aux1_parteDecimal);
                    resultadoFinal = signo * resultadoFinal;
                }
            }
            else
            {
                resultadoFinal = float.Parse(test1[0]);
                
            }

            return resultadoFinal;

        }

        public Log.ILoggerCustom Logger
        {
            get
           ;
            set
            ;
        }


      
    }

   
}
