﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.UpdateGMT
{
    /// <summary>
    /// Contrato que representa la administracion de los paises
    /// y su actualización de GMT.
    /// </summary>
    public interface IManageCountry
    {
        /// <summary>
        /// Devuelve todos los paises involucrados en el GMT
        /// </summary>
        /// <returns></returns>
        System.Data.DataTable GetAllCountry();
        /// <summary>
        /// Asocia los valores GMT en los paises indicados.
        /// </summary>
        /// <param name="namePais">Nombre clave del pais/region</param>
        /// <param name="NewGmt">Valor de GMT que se asocia a "namePais"</param>
        void UpdateGMT_ByNamePais(string namePais, float NewGmt);
        /// <summary>
        /// Sincroniza GMT de Paises/Regiones encontrados con alguna otro medio principal.
        /// </summary>
        void SincronizarTableHorarioRegion();
    }
}
