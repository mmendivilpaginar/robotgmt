﻿using System;
namespace com.paginar.johnson.UpdateGMT
{
    interface IParserByGMT<TIPO_RESULT, TIPO_RETURN> where TIPO_RESULT : Generic.ObjectResult<TIPO_RETURN>
    {
        /// <summary>
        /// Devuelve un objeto que representa el origen de las expresiones regulares
        /// </summary>
         com.paginar.johnson.UpdateGMT.XmlReader.IMatches_File SourceExpRegular
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueInText">El valor en texto que se procesara y analizara</param>
        /// <returns>Devuelve el resultado del parser envuelto en un objeto ObjectResult</returns>
         TIPO_RESULT ParserResult(string valueInText);
    }
}
