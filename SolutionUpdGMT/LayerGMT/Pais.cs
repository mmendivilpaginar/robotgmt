﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.UpdateGMT.Generic;

namespace com.paginar.johnson.UpdateGMT
{
    internal class Pais:IImplementLogger 
    {
        private IQueryGMT<ObjectResult<float?>,float?> CalculadorGMT=null;
        private Boolean IsGMTChanged=false;
        public IManageCountry ManagerPaisDA;

        public Pais(string _Nombre, float _GMT, IQueryGMT<ObjectResult<float?>, float?> _CalculadorGMT)
        {
            this.Nombre = _Nombre;
            this.GMT = _GMT;
            this.CalculadorGMT = _CalculadorGMT;
        }

        public string Nombre
        {
            get;
            private set;

        }

        private float _OldValueGMT;
        private float _gmt;
        public float GMT
        {
            get{
                return _gmt;
            
            }
        
           private set
            {
                if (this._gmt != value)
                {
                    this._OldValueGMT = this._gmt;
                    this._gmt = value;
                    IsGMTChanged = true;
                }
                else
                    IsGMTChanged = false;

            }
        }

        public void ActualizarGMT()
        {
            ObjectResult<float?> value = this.CalculadorGMT.GetValueGMT(this.Nombre);
            if (value.IsValid)
                this.GMT = value.Result.Value;

        }

        public void Guardar()
        {
            if (this.IsGMTChanged)
            {
                this.ManagerPaisDA.UpdateGMT_ByNamePais(this.Nombre, this.GMT);
                this.Logger.Log(string.Format("Cambio GMT para {0}, de {1} a {2}", this.Nombre, this._OldValueGMT, this.GMT));
                
            }
            else
                this.Logger.Log(string.Format("No hubo cambios de GMT para {0}", this.Nombre));
            //throw new System.NotImplementedException();
        }

        public Log.ILoggerCustom Logger
        {
            get
           ;
            set
            ;
        }
    }
}
