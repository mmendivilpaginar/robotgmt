﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace com.paginar.johnson.UpdateGMT.DA
{
   public class ManagerDB
    {

       public ManagerDB()
       {



       }

       public System.Data.DataTable ExecuteSelect(string _select)
       {
           System.Data.DataTable table=null;
           using (SqlConnection cnn=new SqlConnection(this.GetConnectionString()))
           {
               cnn.Open();
               SqlCommand cmd = new SqlCommand(_select, cnn);
               SqlDataReader sqlReader = cmd.ExecuteReader();
               table = new System.Data.DataTable();
               table.Load(sqlReader);
           }

           return table;
       }

       public void ExecuteUpdate(string _sqlUpdate)
       {
           using (SqlConnection cnn = new SqlConnection(this.GetConnectionString()))
           {
               cnn.Open();
               SqlCommand cmd = new SqlCommand(_sqlUpdate, cnn);
               cmd.ExecuteNonQuery();
            
           }
       }

       private string GetConnectionString()
       {
           return System.Configuration.ConfigurationManager.ConnectionStrings["MyConnectionGMT"].ConnectionString;
       }
   }
}
