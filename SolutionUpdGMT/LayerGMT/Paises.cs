﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.UpdateGMT.Generic;

namespace com.paginar.johnson.UpdateGMT
{
	internal class Paises:IImplementLogger
	{
		private List<com.paginar.johnson.UpdateGMT.Pais> PaisCollection
		{
			get
		   ;
			set
			;
		}

        public IManageCountry ManagerPaisForDA;

        /// <summary>
        /// Actualiza los valores GMT para los paises/regiones indicados
        /// usando el objeto (IQueryGMT) pasado como parámetro.
        /// </summary>
        /// <param name="tablePaises">Todos los paises recuperados para actualizar GMT.</param>
        /// <param name="_robotGMT">Objeto que consigue los GMT para los paises.</param>
        public void GMT_UpdatePaises(System.Data.DataTable tablePaises, IQueryGMT<ObjectResult<float?>, float?> _robotGMT)
		{
			PaisCollection = this.FillCollectionPais(tablePaises, _robotGMT);

		
			foreach (var item in PaisCollection)
			{
				item.ActualizarGMT();
                item.Guardar();
		 
			}
			
		}



        private List<Pais> FillCollectionPais(System.Data.DataTable tablePaises, IQueryGMT<ObjectResult<float?>,float?> _robotGMT)
		{
		   List<Pais>  _PaisCollection = new List<Pais>();
			foreach (System.Data.DataRow item in tablePaises.Rows)
			{
				if (!item.IsNull("UrlQuery"))
				{
					string _nombre = item["Pais"].ToString();
					float _gmt = (float)Convert.ToDouble(item["GMT"]);
					_PaisCollection.Add(this.CrearPais(_nombre, _gmt, _robotGMT));
				}


			}

			return _PaisCollection;
		}

        private Pais CrearPais(string nombre, float gmt, IQueryGMT<ObjectResult<float?>,float?> CalculeGMT)
		{

            return new Pais(nombre, gmt, CalculeGMT) {  ManagerPaisDA=this.ManagerPaisForDA, Logger = this.Logger};
		}

        public Log.ILoggerCustom Logger
        {
            get
          ;
            set
           ;
        }
    }
}
