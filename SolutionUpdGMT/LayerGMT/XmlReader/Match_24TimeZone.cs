﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace com.paginar.johnson.UpdateGMT.XmlReader
{
   /// <summary>
   /// Gestiona el acceso a las expresiones regulares asociadas a su archivo del mismo nombre 
   /// que la clase.
   /// </summary>
    public class Match_24TimeZone:IMatches_File
    {
      /// <summary>
      /// Devuelve el nombre del archivo xml asociado para extraer las expresiones regulares.
      /// Se sigue el principio de Convencion sobre configuracion.
      /// </summary>
       private string FileNameDocumentMatchXML// = "match_regex.xml";
       {
           // Principio: Convencion sobre configuracion
            get
           {
               string val = this.GetType().Name + ".xml";
               return val;
           }
       }

       #region Fields
       private string IdiomaSettting = "";
       XDocument _DocumentMatches = null;

       #endregion


       /// <summary>
       /// Devuelve el documento xml asociado a esta clase que contiene las expresiones regulares
       /// estructuradas.
       /// Se sigue el principio "Convention Over Configuration"
       /// </summary>
       /// <returns></returns>
        private XDocument GetXmlOfRegularExpression()
        {
                if (_DocumentMatches == null)
                {
                    string xml_string=System.IO.File.ReadAllText(FileNameDocumentMatchXML);
                    _DocumentMatches = XDocument.Parse(xml_string);
                }
                return _DocumentMatches;
        }

        public Match_24TimeZone(string idioma)
        {
            IdiomaSettting = idioma.ToUpper();
        }

       /// <summary>
       /// Busca la expresion regular en el origen de datos y la devuelve
       /// </summary>
       /// <param name="ini_idioma">El idioma a que corresponde la expresion regular</param>
       /// <param name="value_FilterLugar">si se trata de una expresion regular Outer o Inner</param>
       /// <param name="typeVerano_Standar">Si es un horario de verano o estandar</param>
       /// <returns></returns>
        private string GetExpresionRegular_By_IdiomaAndType(string ini_idioma, string value_FilterLugar, string typeVerano_Standar)
        {
            XDocument DocumentMatches = this.GetXmlOfRegularExpression();

            var res = from row in DocumentMatches.Descendants("idioma")
                      where row.Attribute("type").Value.ToUpper().Equals(ini_idioma.ToUpper())
                      from row2 in row.Descendants("expregularhorario")
                      where row2.Attribute("type").Value.ToUpper().Equals(typeVerano_Standar.ToUpper())
                      from row3 in row2.Descendants("expresion")
                      where row3.Attribute("type").Value.ToUpper().Equals(value_FilterLugar.ToUpper())
                      select new { ExpReg_Value = row3.Attribute("value").Value };
            var ret1 = res.FirstOrDefault();

            if (res.Count() > 0)
                return ret1.ExpReg_Value;
            else
                return string.Empty;
        }



       private string ConvetTipoHorarioAString(Enum_TipoHoraio tipo_horario)
        {
            string value_TipoExpresionRegular = "";
            switch (tipo_horario)
            {
                case Enum_TipoHoraio.Horario_Verano:
                    value_TipoExpresionRegular = "verano";
                    break;
                case Enum_TipoHoraio.Horario_Standar:
                    value_TipoExpresionRegular = "standar";
                    break;

                default:
                    break;
            }

            return value_TipoExpresionRegular;
        }

        #region IMatches_File

        public string Get_ExpRegularInner(Enum_TipoHoraio tipo_horario)
        {
            string value_FilterLugar = "inner";
            string value_TipoExpresionRegular = ConvetTipoHorarioAString(tipo_horario);
           
            string StrExpReg = GetExpresionRegular_By_IdiomaAndType(IdiomaSettting, value_FilterLugar,value_TipoExpresionRegular);

            return StrExpReg;

        }

        public string Get_ExpRegularOuter(Enum_TipoHoraio tipo_horario)
        {
            string value_FilerTypeExp = "outer";
            string value_TipoExpresionRegular = ConvetTipoHorarioAString(tipo_horario);

            string StrExpReg = GetExpresionRegular_By_IdiomaAndType(IdiomaSettting, value_FilerTypeExp, value_TipoExpresionRegular);

            return StrExpReg;
        }

        #endregion

      
    }
}
