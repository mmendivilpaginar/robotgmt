﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.UpdateGMT.XmlReader
{
  /// <summary>
  /// Representa un contrato que tiene los metodos necesarios
  /// para extraer de algun medio, las expresions regulares necesarias
  /// para procesar contenidos de texto.
  /// </summary>
    internal  interface IMatches_File
    {
      /// <summary>
      /// Devuelve la expresion regular que analizará la parte más exterior del valor buscado
      /// </summary>
      /// <param name="tipo_horario">de Verano o Standar</param>
      /// <returns>La expresion regular correspondiente</returns>
         string Get_ExpRegularInner( Enum_TipoHoraio tipo_horario );

         /// <summary>
         /// Devuelve la expresion regular que analizará la parte más interior del valor buscado.
         /// </summary>
         /// <param name="tipo_horario">de Verano o Standar</param>
         /// <returns>La expresion regular correspondiente</returns>
         string Get_ExpRegularOuter(Enum_TipoHoraio tipo_horario);
    }
}
