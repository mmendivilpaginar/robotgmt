﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace com.paginar.johnson.UpdateGMT
{
    internal class ManageDBPaises : IManageCountry
    {
        public System.Data.DataTable GetAllCountry()
        {

            DA.ManagerDB MyManager = new DA.ManagerDB();
            DataTable tableMy=MyManager.ExecuteSelect("SELECT Pais, GMT, UrlQuery,Idioma FROM  dbo.LastGMT_ByCountry");

            return tableMy;

           
        }


        public void UpdateGMT_ByNamePais(string namePais, float NewGmt)
        {
            DA.ManagerDB MyManager = new DA.ManagerDB();
            string sql_upd = string.Format("Update dbo.LastGMT_ByCountry set GMT={0} where Pais='{1}'", NewGmt, namePais);
            MyManager.ExecuteUpdate(sql_upd);
        }


        public void SincronizarTableHorarioRegion()
        {
            DA.ManagerDB MyManager = new DA.ManagerDB();

            StringBuilder sb = new StringBuilder();
            sb.Append("Update T set T.gmt=S.GMT from  [dbo].[RegionPais] T inner join dbo.LastGMT_ByCountry S ");
            sb.Append("on T.Descripcion=S.Pais");
            MyManager.ExecuteUpdate(sb.ToString());
            

        }
    }
}
