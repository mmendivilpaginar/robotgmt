﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using com.paginar.johnson.UpdateGMT.Log;
using System.Globalization;
using System.Threading;
using System.Diagnostics;
using com.paginar.johnson.UpdateGMT.Generic;

namespace com.paginar.johnson.UpdateGMT
{
    public struct Struct_Url_Idioma
    {
        public string Url;
        public string Idioma;
    }
    public class ManagerGMT:IImplementLogger
    {
        private Dictionary<String, Struct_Url_Idioma> _Dictionary_UrlByCountry;

        
        /// <summary>
        /// Ejecuta la coordinacion de todos los pasos necesarios para actualizar los GMT.
        /// </summary>
        public void Actualizar()
        {
            Stopwatch sp = new Stopwatch();
            sp.Start();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            IManageCountry MyReadCountry = new ManageDBPaises();

            //--Traer paises
            this.Logger.Log("Extrayendo listado de paises...");
            DataTable tablePais=MyReadCountry.GetAllCountry();
            
            //--Crear diccionario Pais->Url WebService,Idioma
            _Dictionary_UrlByCountry = this.CreateDictionary(tablePais);

            Paises listadoPaises = new Paises() { ManagerPaisForDA = MyReadCountry, Logger = this.Logger};

            //-- Acoplar Parser de los resultados
            IParserByGMT<ObjectResult<string>, string> _parserResult = new _ParserByGMT() { Logger = this.Logger };
            
        
            //--Crear Robot que trae los GMT por Pais. Indicar cual es el parser que usará

            IQueryGMT<ObjectResult<float?>,float?> _robot = new GMT_From24TimeZone(_Dictionary_UrlByCountry) { ParserHTML = _parserResult, Logger = this.Logger };

            //-- Actualizar GMT de Paises segun lo traido  usando _robot
            this.Logger.Log("Actualizando a nuevos GMT...");
            listadoPaises.GMT_UpdatePaises(tablePais, _robot );

            this.Logger.Log("Sincronizando tabla temporal con Region para GMT");
            
            MyReadCountry.SincronizarTableHorarioRegion();

            this.Logger.Log("Sincronizado tabla temporal con Region finalizada.");

            sp.Stop();

            this.Logger.Log(string.Format("Tiempo de procesamiento: {0:0.##} minutos.", sp.Elapsed.TotalMinutes));
            
            this.Logger.SendSummary();
        }

        private Dictionary<string, Struct_Url_Idioma> CreateDictionary(DataTable tablePais)
        {
            Dictionary<string, Struct_Url_Idioma> _dicRet = new Dictionary<string, Struct_Url_Idioma>();
            string _urlquery="";
            string _idiomaQuery="";
            foreach (DataRow item in tablePais.Rows)
            {
                
                if(!item.IsNull("UrlQuery"))
                {   _urlquery=item["UrlQuery"].ToString();
                    _idiomaQuery=item["Idioma"].ToString();

                    Struct_Url_Idioma _value = new Struct_Url_Idioma() { Url = _urlquery, Idioma = _idiomaQuery };
                    _dicRet.Add(item["Pais"].ToString(),_value);
                }

                
            }

            return _dicRet;
        }





        private ILoggerCustom _Logger;

        public ILoggerCustom Logger
        {
            get { return _Logger; }
            set { _Logger = value; }
        }
       
    }
}
