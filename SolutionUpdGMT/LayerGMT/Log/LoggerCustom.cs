﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.UpdateGMT.Log.Mail;

namespace com.paginar.johnson.UpdateGMT.Log
{
    public class LoggerCustom:ILoggerCustom
    {
        StringBuilder _AccumulateMessage;

        public StringBuilder AccumulateMessage
        {
            get {
                if (_AccumulateMessage == null) _AccumulateMessage = new StringBuilder();
                return  _AccumulateMessage; 
            }
           
        }
        public void Log(string text)
        {
            string valueText = string.Format("> {0}", text);
            string valueTextHtml = string.Format("<div>&gt; {0}</div>", text); 
            Console.WriteLine(valueText);
            //this.AccumulateMessage.Append(valueText);
            this.AccumulateMessage.Append(valueTextHtml);


        }

        public void SendMail(string cuerpo)
        {
            string PATH_FILE_CONFIG = "WebNotify.xml";

           // cuerpo = this.WrapperWithHTML(cuerpo);
            IContentsMail ic = new ContentsText(cuerpo);

            IDataRetriever idr = new ConfigurationDataRetriever(PATH_FILE_CONFIG);
            
           // IFilter myFilter = new FilterByMail(idr);

            Mail.Mail mailSend = new Mail.Mail(idr, ic);// { SourceFilter = myFilter };

            mailSend.EnviarNotificacion();
        }

       

        public void SendSummary()
        {
            this.SendMail(this.AccumulateMessage.ToString());
        }



        public void SendMail(string asunto, string cuerpo, string destino)
        {
            throw new NotImplementedException();
        }
    }
}
