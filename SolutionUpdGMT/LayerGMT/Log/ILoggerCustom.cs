﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.UpdateGMT.Log
{
   /// <summary>
   /// Contrato basico que representa objetos de Logging
   /// </summary>
    public interface ILoggerCustom
    {
       /// <summary>
       /// Deja una traza del contenido "text" en un destino configurado.
       /// </summary>
       /// <param name="text">El contenido de texto a trazar.</param>
       void Log(string text);
       /// <summary>
       /// Envia un correo el contenido indicado, con el asunto y 
       /// a los destinos configurados.
       /// </summary>
       /// <param name="cuerpo">Contenido del cuerpo del correo</param>
       void SendMail(string cuerpo);
       /// <summary>
       /// Envia un correo electronico con el asunto, cuerpo y destinatarios indicados.
       /// </summary>
       /// <param name="asunto">Titulo del correo</param>
       /// <param name="cuerpo">El contenido en si que se enviará</param>
       /// <param name="destino">Los destinatarios de correo separados por ","</param>
       void SendMail(string asunto,string cuerpo,string destino);
       /// <summary>
       /// Envia por algun medio toda la traza del proceso realizado.
       /// </summary>
       void SendSummary();
    }
}
