﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.UpdateGMT.Log.Mail
{
    /// <summary>
    /// Contrato que agrupo el contenido que se enviara por correo.
    /// </summary>
   public interface IContentsMail
    {
       /// <summary>
       /// Representa el cuerpo del correo en html
       /// </summary>
 
       string TextToSend { get;}
       /// <summary>
       /// Representa el listado de archivos adjuntos asociados.
       /// </summary>
        List<System.Net.Mail.Attachment> Adjuntos{get;}
    }
}
