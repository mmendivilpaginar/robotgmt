﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.UpdateGMT.Log.Mail
{
   /// <summary>
   /// Contrato que agrupa las propiedades basicas para gestionar comunicacion
   /// por correo.
   /// </summary>
    public interface IDataRetriever
    {

       Boolean EstaEnProduccion { get; }
        
        string User { get; }

        string Asunto
       {
           get;
       }
        string Port
       {
           get
           ;
       }

        string From
       {
           get
           ;
       }

        string To
       {
           get
           ;
       }

        string SmtpServer
       {
           get
           ;
       }


        string Password
       {
           get;
       }

        Boolean Enable_Ssl
       {
           get;
       }


      
    }
}
