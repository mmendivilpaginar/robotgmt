﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.UpdateGMT.Log.Mail
{
    /// <summary>
    /// Contrato que representa una validacion para contenidos.
    /// </summary>
   public interface IFilter
    {
       /// <summary>
       /// Devuelve un valor booleano indicando si se pasaron todas las reglas de validacion
       /// </summary>
       /// <returns></returns>
        Boolean IsValid();
    }
}
