﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.UpdateGMT.Log.Mail
{
   public class ContentsText:IContentsMail
    {
       private string _TextToSend; 
       public string TextToSend
        {
            get { 
                return _TextToSend; 
            }
        }

        public List<System.Net.Mail.Attachment> Adjuntos
        {
            get { return null; }
        }


        public ContentsText(string txtSubject)
        {
            this._TextToSend = txtSubject;
        }
    }
}
