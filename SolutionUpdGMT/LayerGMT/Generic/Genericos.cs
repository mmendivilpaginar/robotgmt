﻿using System;
namespace com.paginar.johnson.UpdateGMT.Generic
{
    public class ObjectResult<T>
    {
        private Boolean _IsValid;
        /// <summary>
        /// Indica si el resultado es valido o existe.
        /// </summary>
        public Boolean IsValid { get {return _IsValid ;} }
        private T _Result;
        /// <summary>
        /// Representa el valor del 'resultado'.
        /// </summary>
        public T Result { get { return _Result; } }

        /// <summary>
        /// Constructor e inicializador de las propiedades IsValid y Result
        /// </summary>
        /// <param name="_result">el valor propiamente dicho del resultado</param>
        /// <param name="_isValid">indica si el valor del resultado el válido</param>
        public ObjectResult(T _result, Boolean _isValid)
        {
            _Result = _result;
            _IsValid = _isValid;
        }

    }
}