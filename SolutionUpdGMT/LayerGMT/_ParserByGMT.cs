﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using com.paginar.johnson.UpdateGMT.Generic;

namespace com.paginar.johnson.UpdateGMT
{
    internal class _ParserByGMT : com.paginar.johnson.UpdateGMT.IParserByGMT<ObjectResult<string>,string>,IImplementLogger
    {
        public com.paginar.johnson.UpdateGMT.XmlReader.IMatches_File SourceExpRegular
        {
             get;
            set;
        }

        private Regex CreateRegexByString(string strReg)
        {
            Regex regex = new Regex(strReg, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            return regex;
        }


        public ObjectResult<string> ParserResult(string resultCompleto)
        {

            try
            {
                string resultProcess = AplicarExpressionByFiltro(resultCompleto, XmlReader.Enum_TipoHoraio.Horario_Verano);
                if (string.IsNullOrEmpty(resultProcess)) // no huboResultados de horario de Verano
                {
                    resultProcess = AplicarExpressionByFiltro(resultCompleto, XmlReader.Enum_TipoHoraio.Horario_Standar);
                    ObjectResult<string> resultFinal = new ObjectResult<string>(resultProcess, !string.IsNullOrEmpty(resultProcess));

                    return resultFinal;
                }
                else //- Sí hubo resultados de horario Verano
                {
                    ObjectResult<string> resultFinal = new ObjectResult<string>(resultProcess, true);
                    return resultFinal;
                }
            }
            catch (Exception err)
            {
                string str = string.Format("Ocurrio un error al analizar texto con expresiones regulares. Error: {0}",err.Message);

                this.Logger.Log(str);
                
                return new ObjectResult<string>("", false);
            }
  
        }

        private string AplicarExpressionByFiltro(string resultCompleto, XmlReader.Enum_TipoHoraio enum_Filter)
        {
            string _strOut = this.SourceExpRegular.Get_ExpRegularOuter(enum_Filter);
            string _strInner = this.SourceExpRegular.Get_ExpRegularInner(enum_Filter);

            Regex _Outer = this.CreateRegexByString(_strOut);
            Match mOuter = _Outer.Match(resultCompleto);
            string resultProcess = "";
            if (mOuter.Success)
            {
                string valueOuter = mOuter.Groups["GMT"].Value;
                Regex _inner = this.CreateRegexByString(_strInner);
                Match mInner = _inner.Match(valueOuter);

                if (mInner.Success)
                {
                    resultProcess = mInner.Groups["ValueGMT"].Value;
                }
            }
            return resultProcess;
        }

        public Log.ILoggerCustom Logger
        {
            get
           ;
            set
            ;
        }
    }
}
