﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.UpdateGMT
{
    /// <summary>
    /// Contrato que representa los objetos que obtienen los GMT.
    /// </summary>
    /// <typeparam name="TIPO_RESULT">Objetos particularizados para resultados</typeparam>
    /// <typeparam name="TIPO_RETURN"></typeparam>
    public interface IQueryGMT<TIPO_RESULT, TIPO_RETURN> where TIPO_RESULT : Generic.ObjectResult<TIPO_RETURN>
    {
        /// <summary>
        /// Encuentra el valor del GMT segun el parametro de pais que se indica
        /// </summary>
        /// <param name="QueryCountry">El valor clave el pais del que se busca su GMT</param>
        /// <returns>Devuelve el valor GMT envuelto en ObjectResult. IsValid indicara si es válido o no.</returns>
        TIPO_RESULT GetValueGMT(string QueryCountry);
    }

   
    
}
