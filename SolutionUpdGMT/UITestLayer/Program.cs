﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.RunUpdGMT
{
    class Program
    {
        static void Main(string[] args)
        {
            com.paginar.johnson.UpdateGMT.Log.ILoggerCustom myLog = new com.paginar.johnson.UpdateGMT.Log.LoggerCustom();
            myLog.Log(string.Format("Iniciando syncronizador GMT ({0})",DateTime.Now));
            Console.WriteLine();
            com.paginar.johnson.UpdateGMT.ManagerGMT ManGMT = new com.paginar.johnson.UpdateGMT.ManagerGMT();
            ManGMT.Logger = myLog;

            myLog.Log("Actualizando Horario GMT de paises de la intranet...");
            Console.WriteLine();
            ManGMT.Actualizar();
            myLog.Log(string.Format("Finalizando sincronizador GMT ({0})", DateTime.Now));
         
            
        }
    }
}
